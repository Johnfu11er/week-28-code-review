from week_28 import greetings

def test_greetings():
    assert greetings("John") == "Hello John"
